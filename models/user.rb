# frozen_string_literal: true

class User
  include Validable

  attr_accessor :username, :age

  validate :username, presence: true, format: /^[A-Za-z\-_ ]+$/
  validate :age, type: Integer
  validate :age, presence: true
end
