# frozen_string_literal: true

require './init'
require_all 'models/*'

# Create class instance and try the validations functional
user = User.new
user.username = 'Ivan'
user.age = 16
user.validate!
puts user.valid?

user2 = User.new
# user2.username = ''
user2.age = 'Hello!'
user2.validate!
puts user2.valid?
