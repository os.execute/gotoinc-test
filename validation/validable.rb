# frozen_string_literal: true

require_relative './validation'
require_relative './validation_error'

module Validable
  def self.included(host_class)
    host_class.extend ClassMethods
  end

  module ClassMethods
    attr_reader :validations

    def validate(field, params)
      @validations ||= []

      params.each do |type, param|
        @validations << Validation.new(field, type, param)
      end
    end
  end

  def validate!
    errors = []

    self.class.validations.each do |validation|
      value = instance_variable_get("@#{validation.attribute}".to_sym)
      error = validation.check(value)

      errors << error if error
    end

    raise ValidationError, errors if errors.any?
  end

  def valid?
    validate!
    true
  rescue ValidationError
    false
  end
end
