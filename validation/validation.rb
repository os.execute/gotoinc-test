# frozen_string_literal: true

class Validation
  attr_accessor :attribute, :type, :param

  def initialize(attribute, type, param)
    @attribute = attribute
    @type = type
    @param = param
  end

  def check(value)
    check_method = "check_#{type}"
    send(check_method, value, param)
  rescue NoMethodError
    puts "The '#{type}' validation is not implemented"
  end

  private

  def check_presence(value, param)
    if param
      "The :#{attribute} is not present" if value.nil? || value == ''
    else
      "The :#{attribute} is present" if !value.nil? && value != ''
    end
  end

  def check_type(value, param)
    "The :#{attribute} shold be '#{param}', but it is '#{value.class}'" unless value.instance_of? param
  end

  def check_format(value, param)
    return unless value

    "The :#{attribute} does not match '#{param.inspect}'" unless value.match? param
  end

  def check_min(value, param)
    "The value is #{value} which is less than minimum #{param}" unless value.is_a?(Numeric) && value >= param
  end
end
