# frozen_string_literal: true

describe 'Presence Validation' do
  describe 'presence: true' do
    let(:presence_class) do
      Class.new do
        include Validable

        attr_accessor :username

        validate :username, presence: true
      end
    end

    before do
      stub_const('PresenceClass', presence_class)
    end

    it 'valid if value is present' do
      instance = PresenceClass.new
      instance.username = 'Good Username'

      expect { instance.validate! }.not_to raise_error
      expect(instance.valid?).to eq(true)
    end

    it 'valid if value is false' do
      instance = PresenceClass.new
      instance.username = false

      expect { instance.validate! }.not_to raise_error
      expect(instance.valid?).to eq(true)
    end

    it 'invalid if value is not present' do
      instance = PresenceClass.new

      expect { instance.validate! }.to raise_error(ValidationError)
      expect(instance.valid?).to eq(false)
    end

    it 'invalid if value is empty' do
      instance = PresenceClass.new
      instance.username = ''

      expect { instance.validate! }.to raise_error(ValidationError)
      expect(instance.valid?).to eq(false)
    end
  end

  describe 'presence: false' do
    let(:presence_class) do
      Class.new do
        include Validable

        attr_accessor :error

        validate :error, presence: false
      end
    end

    before do
      stub_const('PresenceClass', presence_class)
    end

    it 'valid if value is nil' do
      instance = PresenceClass.new
      instance.error = nil

      expect { instance.validate! }.not_to raise_error
      expect(instance.valid?).to eq(true)
    end

    it 'valid if value is not present' do
      instance = PresenceClass.new

      expect { instance.validate! }.not_to raise_error
      expect(instance.valid?).to eq(true)
    end

    it 'invalid if value is present' do
      instance = PresenceClass.new
      instance.error = 'Yes, this is an error'

      expect { instance.validate! }.to raise_error(ValidationError)
      expect(instance.valid?).to eq(false)
    end

    it 'invalid if value is false' do
      instance = PresenceClass.new
      instance.error = false
      expect { instance.validate! }.to raise_error(ValidationError)
      expect(instance.valid?).to eq(false)
    end
  end
end
