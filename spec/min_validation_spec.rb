# frozen_string_literal: true

describe 'Min Validation' do
  describe 'min: 18' do
    let(:min_class) do
      Class.new do
        include Validable

        attr_accessor :age

        validate :age, min: 18
      end
    end

    before do
      stub_const('MinClass', min_class)
    end

    it 'valid if value is above minimum' do
      instance = MinClass.new
      instance.age = 21

      expect { instance.validate! }.not_to raise_error
      expect(instance.valid?).to eq(true)
    end

    it 'invalid if value is below minimum' do
      instance = MinClass.new
      instance.age = 17

      expect { instance.validate! }.to raise_error(ValidationError)
      expect(instance.valid?).to eq(false)
    end

    it 'invalid if value is not a number' do
      instance = MinClass.new
      instance.age = 'Hello!'

      expect { instance.validate! }.to raise_error(ValidationError)
      expect(instance.valid?).to eq(false)
    end
  end
end
