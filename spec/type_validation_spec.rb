# frozen_string_literal: true

describe 'Type Validation' do
  describe 'type: Integer' do
    let(:type_class) do
      Class.new do
        include Validable

        attr_accessor :age

        validate :age, type: Integer
      end
    end

    before do
      stub_const('TypeClass', type_class)
    end

    it 'valid if value is matching type' do
      instance = TypeClass.new
      instance.age = 333

      expect { instance.validate! }.not_to raise_error
      expect(instance.valid?).to eq(true)
    end

    it 'invalid if value is not matching type' do
      instance = TypeClass.new
      instance.age = 'Hello!'

      expect { instance.validate! }.to raise_error(ValidationError)
      expect(instance.valid?).to eq(false)
    end
  end
end
