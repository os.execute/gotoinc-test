# frozen_string_literal: true

describe 'Format Validation' do
  describe 'format: /^[A-Za-z\- ]+$/' do
    let(:format_class) do
      Class.new do
        include Validable

        attr_accessor :name

        validate :name, format: /^[A-Za-z\- ]+$/
      end
    end

    before do
      stub_const('FormatClass', format_class)
    end

    it 'valid if value is matching regexp' do
      instance = FormatClass.new
      instance.name = 'Hellowich Wordow'

      expect { instance.validate! }.not_to raise_error
      expect(instance.valid?).to eq(true)
    end

    it 'invalid if value is not matching regexp' do
      instance = FormatClass.new
      instance.name = 'Hello 123'

      expect { instance.validate! }.to raise_error(ValidationError)
      expect(instance.valid?).to eq(false)
    end
  end
end
