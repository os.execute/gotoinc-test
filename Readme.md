# Implementation of the custom 'Validable' module

### To test:
1. Use 'rspec' to run some of the rspec defined tests
2. Use `~ ruby ./run_validations.rb` to run the code that defines a class and straightforwardly tries the validations
